package com.perpetualwave.medtronic.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.perpetualwave.medtronic.R;
import com.perpetualwave.medtronic.adapters.ProductAdapter;
import com.perpetualwave.medtronic.models.Product;
import com.perpetualwave.medtronic.utils.ProductListMgr;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProductSearchActivity extends AppCompatActivity {

    @Bind(R.id.txtProductSearchName)
    TextView txtProductSearchName;
    @Bind(R.id.txtProductSearchEquivalent)
    TextView txtProductSearchEquivalent;
    @Bind(R.id.edtProductSearchSearch)
    EditText edtProductSearchSearch;
    @Bind(R.id.imgProductSearchSearch)
    ImageView imgProductSearchSearch;
    @Bind(R.id.imgProductSearchScan)
    ImageView imgProductSearchScan;
    @Bind(R.id.rvProductSearchSimilarProducts)
    RecyclerView rvProductSearchSimilarProducts;
    @Bind(R.id.btnProductSearchDone)
    Button btnProductSearchDone;

    private Context mContext;
    private ProductAdapter mAdapter;

    public static ProductListMgr.MatchResult matchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        mContext = getApplicationContext();

        ButterKnife.bind(this);
        setupViews();
    }

    private void setupViews() {
        imgProductSearchScan.setOnClickListener(startScannerListener);
        imgProductSearchSearch.setOnClickListener(searchListener);
        btnProductSearchDone.setOnClickListener(doneListener);
        txtProductSearchName.setText(matchResult.closestProduct.getProduct());
        txtProductSearchEquivalent.setText("This is the Medtronic equivalent of " + matchResult.closestProduct.getEthiconBrand());

        resetList();
    }

    private void resetList() {
        mAdapter = new ProductAdapter(matchResult.similarProducts);
        rvProductSearchSimilarProducts.setLayoutManager(new LinearLayoutManager(mContext));
        rvProductSearchSimilarProducts.setAdapter(mAdapter);
    }

    View.OnClickListener doneListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    View.OnClickListener startScannerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(mContext, ImageCaptureActivity.class));
            finish();
        }
    };

    View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ProductListMgr.MatchResult temp = ProductListMgr.getInstance().getSimilarProductsWithString(edtProductSearchSearch.getText().toString());
            if(temp != null){
                matchResult = temp;
                setupViews();
            }
        }
    };
}
