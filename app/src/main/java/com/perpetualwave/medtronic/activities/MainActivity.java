package com.perpetualwave.medtronic.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.perpetualwave.medtronic.R;
import com.perpetualwave.medtronic.adapters.ProductAdapter;
import com.perpetualwave.medtronic.utils.ProductListMgr;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.rvMainProductList)
    RecyclerView rvMainProductList;
    @Bind(R.id.edtMainSearch)
    EditText edtMainSearch;
    @Bind(R.id.imgMainSearch)
    ImageView imgMainSearch;
    @Bind(R.id.imgMainScan)
    ImageView imgMainScan;

    private Context mContext;
    private ProductAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getApplicationContext();

        ButterKnife.bind(this);
        setupViews();

    }

    private void setupViews() {
        imgMainScan.setOnClickListener(startScannerListener);
        imgMainSearch.setOnClickListener(searchListener);
        resetList();
    }

    private void resetList() {
        mAdapter = new ProductAdapter(ProductListMgr.getInstance().getAllProducts());
        rvMainProductList.setLayoutManager(new LinearLayoutManager(mContext));
        rvMainProductList.setAdapter(mAdapter);
    }

    View.OnClickListener startScannerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(mContext, ImageCaptureActivity.class));
        }
    };

    View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mAdapter = new ProductAdapter(ProductListMgr.getInstance().getAllProductsWithQuery(edtMainSearch.getText().toString()));
            rvMainProductList.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    };

}