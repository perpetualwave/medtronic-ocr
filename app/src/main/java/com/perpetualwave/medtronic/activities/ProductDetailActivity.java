package com.perpetualwave.medtronic.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.perpetualwave.medtronic.R;
import com.perpetualwave.medtronic.adapters.ProductAdapter;
import com.perpetualwave.medtronic.models.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jakejacobo on 01/06/2016.
 */
public class ProductDetailActivity extends  AppCompatActivity {

    @Bind(R.id.txtProductName)
    TextView txtProductName;

    @Bind(R.id.txtBrand)
    TextView txtBrand;

    @Bind(R.id.txtMtfCode)
    TextView txtMtfCode;

    @Bind(R.id.txtUSPSize)
    TextView txtUSPSize;

    @Bind(R.id.txtSatureLength)
    TextView txtSatureLength;

    @Bind(R.id.txtMaterial)
    TextView txtMaterial;

    @Bind(R.id.txtProduct)
    TextView txtProduct;

    @Bind(R.id.txtSatureColor)
    TextView txtSatureColor;

    @Bind(R.id.txtNeedleName)
    TextView txtNeedleName;

    @Bind(R.id.txtNeedleCurvature)
    TextView txtNeedleCurvature;

    @Bind(R.id.txtNeedleCrossSection)
    TextView txtNeedleCrossSection;

    @Bind(R.id.txtEthiconBrand)
    TextView  txtEthiconBrand;

    @Bind(R.id.txtNeedleLength)
    TextView txtNeedleLength;



    private Context mContext;



    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.layout_product_detail);
            mContext = getApplicationContext();
            ButterKnife.bind(this);


          Intent intent = getIntent();
         ArrayList<String> StA = intent.getStringArrayListExtra("SelectID");


        TextView []tv=new TextView[13];
        tv[0]=(TextView)findViewById(R.id.txtProductName);
        tv[1]=(TextView)findViewById(R.id.txtBrand);
        tv[2]=(TextView)findViewById(R.id.txtMtfCode);
        tv[3]=(TextView)findViewById(R.id.txtUSPSize);
        tv[4]=(TextView)findViewById(R.id.txtSatureLength);
        tv[5]=(TextView)findViewById(R.id.txtMaterial);
        tv[6]=(TextView)findViewById(R.id.txtProduct);
        tv[7]=(TextView)findViewById(R.id.txtSatureColor);
        tv[8]=(TextView)findViewById(R.id.txtNeedleName);
        tv[9]=(TextView)findViewById(R.id.txtNeedleCurvature);
        tv[10]=(TextView)findViewById(R.id.txtNeedleCrossSection);
        tv[11]=(TextView)findViewById(R.id.txtEthiconBrand);
        tv[12]=(TextView)findViewById(R.id.txtNeedleLength);

        for(int i=0; i < StA.size();i++){
          tv[i].setText(StA.get(i));

        }



        }

    }
