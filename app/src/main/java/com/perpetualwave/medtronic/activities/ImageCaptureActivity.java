package com.perpetualwave.medtronic.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.microblink.activity.BlinkOCRActivity;
import com.microblink.activity.ShowOcrResultMode;
import com.microblink.directApi.DirectApiErrorListener;
import com.microblink.directApi.Recognizer;
import com.microblink.hardware.orientation.Orientation;
import com.microblink.ocr.ScanConfiguration;
import com.microblink.recognition.FeatureNotSupportedException;
import com.microblink.recognition.InvalidLicenceKeyException;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkocr.BlinkOCRRecognitionResult;
import com.microblink.recognizers.blinkocr.BlinkOCRRecognizerSettings;
import com.microblink.recognizers.blinkocr.parser.generic.RawParserSettings;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.microblink.results.ocr.OcrResult;
import com.microblink.view.recognition.ScanResultListener;
import com.perpetualwave.medtronic.Core.CameraEngine;
import com.perpetualwave.medtronic.Core.ExtraViews.FocusBoxView;
import com.perpetualwave.medtronic.Core.Imaging.Tools;
import com.perpetualwave.medtronic.R;
import com.perpetualwave.medtronic.utils.ProductListMgr;


public class ImageCaptureActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener,
        Camera.PictureCallback, Camera.ShutterCallback, ScanResultListener {

    static final int MY_REQUEST_CODE = 1;
    static final String TAG = "DBG_" + MainActivity.class.getName();
    private Recognizer mRecognizer;

    Button shutterButton;
    Button focusButton;
    FocusBoxView focusBox;
    SurfaceView cameraFrame;
    CameraEngine cameraEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showOCRCapture();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        Log.d(TAG, "Surface Created - starting camera");

        if (cameraEngine != null && !cameraEngine.isOn()) {
            cameraEngine.start();
        }

        if (cameraEngine != null && cameraEngine.isOn()) {
            Log.d(TAG, "Camera engine already on");
            return;
        }

        cameraEngine = CameraEngine.New(holder);
        cameraEngine.start();

        Log.d(TAG, "Camera engine started");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        //resetCam();

    }

    @Override
    protected void onStart() {
        super.onStart();
        //initRecognizer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //mRecognizer.terminate();
    }

    private void showOCRCapture(){
        Intent intent = new Intent(this, BlinkOCRActivity.class);
        intent.putExtra(BlinkOCRActivity.EXTRAS_LICENSE_KEY, "32UP5GIX-YNI7S3NW-IYEZ63EM-E4HY53J6-X6D4M2W4-2NPVEEBY-V2OBQLJJ-Z7ZI5T4M");

        ScanConfiguration[] confArray = new ScanConfiguration[] {
                new ScanConfiguration(R.string.raw_title, R.string.raw_msg, "Raw", new RawParserSettings())
        };
        intent.putExtra(BlinkOCRActivity.EXTRAS_SCAN_CONFIGURATION, confArray);
        intent.putExtra(BlinkOCRActivity.EXTRAS_SHOW_OCR_RESULT, false);
        intent.putExtra(BlinkOCRActivity.EXTRAS_SHOW_OCR_RESULT_MODE, (Parcelable) ShowOcrResultMode.STATIC_CHARS);
// Starting Activity
        startActivityForResult(intent, MY_REQUEST_CODE);
    }

    private void initRecognizer() {
        try {
            mRecognizer = Recognizer.getSingletonInstance();
        } catch (FeatureNotSupportedException e) {
            Toast.makeText(this, "Feature not supported! Reason: " + e.getReason().getDescription(), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        try {
            // set license key
            mRecognizer.setLicenseKey(this, "32UP5GIX-YNI7S3NW-IYEZ63EM-E4HY53J6-X6D4M2W4-2NPVEEBY-V2OBQLJJ-Z7ZI5T4M");
        } catch (InvalidLicenceKeyException exc) {
            finish();
            return;
        }
        RecognitionSettings settings = new RecognitionSettings();
        // setupSettingsArray method is described in chapter "Recognition
        // settings and results")
        settings.setRecognizerSettingsArray(setupSettingsArray());
        mRecognizer.initialize(this, settings, new DirectApiErrorListener() {
            @Override
            public void onRecognizerError(Throwable t) {
                Toast.makeText(ImageCaptureActivity.this, "There was an error in initialization of Recognizer: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private RecognizerSettings[] setupSettingsArray() {
        BlinkOCRRecognizerSettings sett = new BlinkOCRRecognizerSettings();

        // add amount parser to default parser group
        sett.addParser("raw", new RawParserSettings());

        // now add sett to recognizer settings array that is used to configure
        // recognition
        return new RecognizerSettings[] { sett };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode == BlinkOCRActivity.RESULT_OK && data != null) {
                // perform processing of the data here

                // for example, obtain parcelable recognition result
                Bundle extras = data.getExtras();
                Bundle results = extras.getBundle(BlinkOCRActivity.EXTRAS_SCAN_RESULTS);

                // results bundle contains result strings in keys defined
                // by scan configuration name
                // for example, if set up as in step 1, then you can obtain
                // e-mail address with following line
                String resultString = results.getString("Raw");
                //Toast.makeText(this, email, Toast.LENGTH_LONG).show();
                ProductListMgr.MatchResult result = ProductListMgr.getInstance().getSimilarProductsWithString(resultString);
                if(result != null){
                    //Toast.makeText(this, "Closest Product: " + result.closestProduct.getProductName(), Toast.LENGTH_LONG).show();
                    ProductSearchActivity.matchResult = result;
                    startActivity(new Intent(this, ProductSearchActivity.class));
                }else {
                    Toast.makeText(this, "Product Not Found", Toast.LENGTH_LONG).show();
                }
                finish();
            }else{
                finish();
            }
        }
    }

    private void resetCam(){
        cameraFrame = (SurfaceView) findViewById(R.id.camera_frame);
        shutterButton = (Button) findViewById(R.id.shutter_button);
        focusBox = (FocusBoxView) findViewById(R.id.focus_box);
        focusButton = (Button) findViewById(R.id.focus_button);

        shutterButton.setOnClickListener(this);
        focusButton.setOnClickListener(this);

        SurfaceHolder surfaceHolder = cameraFrame.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        cameraFrame.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (cameraEngine != null && cameraEngine.isOn()) {
//            cameraEngine.stop();
//        }
//
//        SurfaceHolder surfaceHolder = cameraFrame.getHolder();
//        surfaceHolder.removeCallback(this);
    }

    @Override
    public void onClick(View v) {
        if(v == shutterButton){
            if(cameraEngine != null && cameraEngine.isOn()){
                cameraEngine.takeShot(this, this, this);
            }
        }

        if(v == focusButton){
            if(cameraEngine!=null && cameraEngine.isOn()){
                cameraEngine.requestFocus();
            }
        }
    }

    @Override
    public void onPictureTaken(byte[] data, final Camera camera) {

        Log.d(TAG, "Picture taken");

        if (data == null) {
            Log.d(TAG, "Got null data");
            return;
        }

        Bitmap bmp = Tools.getFocusedBitmap(this, camera, data, focusBox.getBox());

        Log.d(TAG, "Got bitmap");

        Log.d(TAG, "Initialization of TessBaseApi");

        //new TessAsyncEngine().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, this, bmp);
        mRecognizer.recognizeBitmap(bmp, Orientation.ORIENTATION_PORTRAIT, this);
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                cameraEngine.stop();
                cameraEngine.start();
            }
        },100);

    }

    @Override
    public void onShutter() {

    }

    @Override
    public void onScanningDone(@Nullable RecognitionResults recognitionResults) {
        BaseRecognitionResult[] dataArray = recognitionResults.getRecognitionResults();
        if(dataArray != null) {
            for (BaseRecognitionResult baseResult : dataArray) {
                if (baseResult instanceof BlinkOCRRecognitionResult) {
                    BlinkOCRRecognitionResult result = (BlinkOCRRecognitionResult) baseResult;
                    if (result.isValid() && !result.isEmpty()) {
                        String parsedAmount = result.getParsedResult("raw");
                        // note that parsed result can be null or empty even if result
                        // is marked as non-empty and valid
                        if (parsedAmount != null && !parsedAmount.isEmpty()) {
                            Toast.makeText(this, parsedAmount, Toast.LENGTH_LONG).show();
                        }
                        OcrResult ocrResult = result.getOcrResult();
                    }
                }
            }
        }
    }
}