package com.perpetualwave.medtronic.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.perpetualwave.medtronic.R;
import com.perpetualwave.medtronic.activities.ProductDetailActivity;
import com.perpetualwave.medtronic.models.Product;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by perpetualwave on 01/06/16.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    Context mContext;
    public ArrayList<Product> productList;
    public ProductAdapter(ArrayList<Product> productList) {
        this.productList = productList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_product, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Product product = productList.get(position);

        holder.txtProductName.setText(product.getProductName());


        holder.cvProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("echo","DATA CLICKED: " + product.getProductName());

//                String[]  SelectedData =  new String[] {product.getProductName(),product.getBrand()};
                ArrayList<String>  data = new ArrayList<String>();

                data.add(product.getProductName());
                data.add(product.getBrand());
                data.add(product.getMtfCode());
                data.add(product.getUSPSize());
                data.add(product.getSutureLength());
                data.add(product.getMaterial());
                data.add(product.getProduct());
                data.add(product.getSutureColour());
                data.add(product.getNeedleName());
                data.add(product.getNeedleCurvature() + "/ " + product.getNeedleType());
                data.add(product.getNeedleCrossSection()  + "/ " + product.getNeedleCategory());
                data.add(product.getEthiconBrand());
                data.add(product.getNeedleLength());

                Intent productDetailAct = new Intent(mContext,ProductDetailActivity.class);
                productDetailAct.putExtra("SelectID",data);


                mContext.startActivity(productDetailAct);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.cvProduct)
        CardView cvProduct;
        @Bind(R.id.txtProductName)
        TextView txtProductName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
