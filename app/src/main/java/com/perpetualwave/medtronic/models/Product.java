package com.perpetualwave.medtronic.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by perpetualwave on 31/05/16.
 */
public class Product {

    private String mtfCode;
    private String productName;
    private String brand;
    private String product;
    private String material;
    private String uSPSize;
    private String sutureLength;
    private String needleName;
    private String sutureColour;
    private String needleLength;
    private String needleCurvature;
    private String needleType;
    private String needleCrossSection;
    private String needleCategory;
    private String ethiconBrand;

    public Product(JSONObject jsonObject){
        try {
            this.mtfCode = jsonObject.has("Mtf Code") ? jsonObject.getString("Mtf Code") : "";
            this.productName = jsonObject.has("Product Name") ? jsonObject.getString("Product Name") : "";
            this.brand = jsonObject.has("Brand") ? jsonObject.getString("Brand") : "";
            this.product = jsonObject.has("Product") ? jsonObject.getString("Product") : "";
            this.material = jsonObject.has("Material") ? jsonObject.getString("Material") : "";
            this.uSPSize = jsonObject.has("USP Size") ? jsonObject.getString("USP Size") : "";
            this.sutureLength = jsonObject.has("Suture Length") ? jsonObject.getString("Suture Length") : "";
            this.needleName = jsonObject.has("Needle name") ? jsonObject.getString("Needle name") : "";
            this.sutureColour = jsonObject.has("Suture Colour") ? jsonObject.getString("Suture Colour") : "";
            this.needleLength = jsonObject.has("Needle length") ? jsonObject.getString("Needle length") : "";
            this.needleCurvature = jsonObject.has("Needle Curvature") ? jsonObject.getString("Needle Curvature") : "";
            this.needleType = jsonObject.has("Needle Type") ? jsonObject.getString("Needle Type") : "";
            this.needleCrossSection = jsonObject.has("Needle cross section") ? jsonObject.getString("Needle cross section") : "";
            this.needleCategory = jsonObject.has("Needle cross section") ? jsonObject.getString("Needle cross section") : "";
            this.ethiconBrand = jsonObject.has("Ethicon Brand") ? jsonObject.getString("Ethicon Brand") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return
     * The mtfCode
     */
    public String getMtfCode() {
        return mtfCode;
    }

    /**
     *
     * @param mtfCode
     * The Mtf Code
     */
    public void setMtfCode(String mtfCode) {
        this.mtfCode = mtfCode;
    }

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The Product Name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @param brand
     * The Brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     *
     * @return
     * The product
     */
    public String getProduct() {
        return product;
    }

    /**
     *
     * @param product
     * The Product
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     *
     * @return
     * The material
     */
    public String getMaterial() {
        return material;
    }

    /**
     *
     * @param material
     * The Material
     */
    public void setMaterial(String material) {
        this.material = material;
    }

    /**
     *
     * @return
     * The uSPSize
     */
    public String getUSPSize() {
        return uSPSize;
    }

    /**
     *
     * @param uSPSize
     * The USP Size
     */
    public void setUSPSize(String uSPSize) {
        this.uSPSize = uSPSize;
    }

    /**
     *
     * @return
     * The sutureLength
     */
    public String getSutureLength() {
        return sutureLength;
    }

    /**
     *
     * @param sutureLength
     * The Suture Length
     */
    public void setSutureLength(String sutureLength) {
        this.sutureLength = sutureLength;
    }

    /**
     *
     * @return
     * The needleName
     */
    public String getNeedleName() {
        return needleName;
    }

    /**
     *
     * @param needleName
     * The Needle name
     */
    public void setNeedleName(String needleName) {
        this.needleName = needleName;
    }

    /**
     *
     * @return
     * The sutureColour
     */
    public String getSutureColour() {
        return sutureColour;
    }

    /**
     *
     * @param sutureColour
     * The Suture Colour
     */
    public void setSutureColour(String sutureColour) {
        this.sutureColour = sutureColour;
    }

    /**
     *
     * @return
     * The needleLength
     */
    public String getNeedleLength() {
        return needleLength;
    }

    /**
     *
     * @param needleLength
     * The Needle length
     */
    public void setNeedleLength(String needleLength) {
        this.needleLength = needleLength;
    }

    /**
     *
     * @return
     * The needleCurvature
     */
    public String getNeedleCurvature() {
        return needleCurvature;
    }

    /**
     *
     * @param needleCurvature
     * The Needle Curvature
     */
    public void setNeedleCurvature(String needleCurvature) {
        this.needleCurvature = needleCurvature;
    }

    /**
     *
     * @return
     * The needleType
     */
    public String getNeedleType() {
        return needleType;
    }

    /**
     *
     * @param needleType
     * The Needle Type
     */
    public void setNeedleType(String needleType) {
        this.needleType = needleType;
    }

    /**
     *
     * @return
     * The needleCrossSection
     */
    public String getNeedleCrossSection() {
        return needleCrossSection;
    }

    /**
     *
     * @param needleCrossSection
     * The Needle cross section
     */
    public void setNeedleCrossSection(String needleCrossSection) {
        this.needleCrossSection = needleCrossSection;
    }

    /**
     *
     * @return
     * The needleCategory
     */
    public String getNeedleCategory() {
        return needleCategory;
    }

    /**
     *
     * @param needleCategory
     * The Needle Category
     */
    public void setNeedleCategory(String needleCategory) {
        this.needleCategory = needleCategory;
    }

    /**
     *
     * @return
     * The ethiconBrand
     */
    public String getEthiconBrand() {
        return ethiconBrand;
    }

    /**
     *
     * @param ethiconBrand
     * The Ethicon Brand
     */
    public void setEthiconBrand(String ethiconBrand) {
        this.ethiconBrand = ethiconBrand;
    }

}
