package com.perpetualwave.medtronic;

import android.app.Application;

import com.perpetualwave.medtronic.models.Product;
import com.perpetualwave.medtronic.utils.ProductListMgr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 * Created by perpetualwave on 31/05/16.
 */
public class MedtronicApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ProductListMgr.prepareInstance(getApplicationContext());
        ProductListMgr.getInstance().parseProductList(getMockDataJSONString());
        //ProductListMgr.getInstance().printData();
    }

    private String getMockDataJSONString(){
        InputStream is = getResources().openRawResource(R.raw.mockdata);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return writer.toString();
    }
}
