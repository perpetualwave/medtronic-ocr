package com.perpetualwave.medtronic.utils;

import android.content.Context;

import com.perpetualwave.medtronic.models.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by perpetualwave on 31/05/16.
 */
public class ProductListMgr {

    private Context mContext;
    private static ProductListMgr instance;
    private HashMap<String, ArrayList<Product>> productList;
    private ArrayList<Product> allProducts;

    private ProductListMgr(Context context){
        this.mContext = context;
    }

    public static void prepareInstance(Context context){
        if(instance == null){
            instance = new ProductListMgr(context);
        }
    }

    public static ProductListMgr getInstance() {
        if(instance == null){
            throw new NullPointerException();
        }
        return instance;
    }

    public void parseProductList(String jsonString){
        if(productList == null){
            productList = new HashMap<>();

            try {
                JSONArray array = new JSONArray(jsonString);
                for(int x = 0; x < array.length(); x++){
                    JSONObject obj = array.getJSONObject(x);
                    Product prod = new Product(obj);

                    if(prod.getEthiconBrand() != null && !prod.getEthiconBrand().isEmpty()){
                        ArrayList<Product> temp = getListOfProductWithKey(prod.getEthiconBrand());
                        temp.add(prod);
                        productList.put(prod.getEthiconBrand(), temp);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Product> getListOfProductWithKey(String ethiconBrand){
        if(productList.containsKey(ethiconBrand)){
            return productList.get(ethiconBrand);
        }
        return new ArrayList<>();
    }

    public MatchResult getSimilarProductsWithString(String str){
        final int STR_SIZE = 3;
        MatchResult result = new MatchResult();
        ArrayList<Product> similarList = new ArrayList<>();
        str = str.toLowerCase();
        str = str.replaceAll("\n","");
        str = str.replaceAll(" ", "");
        ArrayList<String> toMatch = new ArrayList<>();
        if(str.length() < STR_SIZE){
            toMatch.add(str);
        }
        for(int x = 0; x < str.length(); x++){

            if((x + STR_SIZE) > str.length()){
                break;
            }
            String match = str.substring(x, (x + STR_SIZE));
            toMatch.add(match);
        }
        Product closestProduct = getClosestProduct(toMatch);

        if(closestProduct != null && !closestProduct.getEthiconBrand().isEmpty()){
            for(Product product : productList.get(closestProduct.getEthiconBrand())){
                if(!product.getProductName().equalsIgnoreCase(closestProduct.getProductName()) && product.getEthiconBrand().equalsIgnoreCase(closestProduct.getEthiconBrand())){
                    similarList.add(product);
                }
            }
        }else{
            return null;
        }

        result.similarProducts = similarList;
        result.closestProduct = closestProduct;

        return result;
    }

    private Product getClosestProduct(ArrayList<String> toMatch){
        int numClose = 0, lastClose = 0;
        Product closestProduct = null;
        for(String key : productList.keySet()){
            for(Product product : productList.get(key)){
                numClose = 0;
                for(String match : toMatch) {
                    if (product.getProduct().toLowerCase().contains(match)){
                        numClose++;
                    }
                }
                if(numClose > 0 && closestProduct == null && !product.getEthiconBrand().isEmpty()){
                    closestProduct = product;
                    lastClose = numClose;
                }else if( numClose > 0){
                    if(numClose > lastClose && !product.getEthiconBrand().isEmpty()){
                        closestProduct = product;
                        lastClose = numClose;
                    }
                }
            }
        }
        return closestProduct;
    }

    public void printData(){
        for(String key : productList.keySet()){
            for(Product product : productList.get(key)){
                System.out.println(product.getProduct() + " : " + product.getEthiconBrand());
            }
        }
    }

    public ArrayList<Product> getAllProductsWithQuery(String query) {
        ArrayList<Product> searchedProduct = new ArrayList<>();
        for(String key : productList.keySet()){
            for(Product product : productList.get(key)){
                if(product.getProductName().toLowerCase().contains(query.toLowerCase()) || product.getEthiconBrand().toLowerCase().contains(query.toLowerCase())){
                    searchedProduct.add(product);
                }
            }
        }

        return searchedProduct;
    }

    public ArrayList<Product> getAllProducts() {
        if(allProducts == null){
            allProducts = new ArrayList<>();
            for(String key : productList.keySet()){
                for(Product product : productList.get(key)){
                    allProducts.add(product);
                }
            }
        }
        return allProducts;
    }

    public class MatchResult{
        public ArrayList<Product> similarProducts;
        public Product closestProduct;
    }

}
